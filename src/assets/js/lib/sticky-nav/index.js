import $ from 'jquery'
import debounce from 'lodash.debounce'

export default selector => {
  $(document).ready(() => {
    const setStickyHeader = () => {
      const header = $(selector)
      const sib = header.next()
      const height = header.height()
      sib.css({ marginTop: height })
    }

    setStickyHeader()

    $(window).resize(debounce(setStickyHeader, 250))
    $(window).on('scroll', debounce(setStickyHeader, 250))
  })
}
