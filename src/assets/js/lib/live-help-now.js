import $ from 'jquery'

const initiateLiveHelpNow = () => {
  window.lhnJsSdkInit = function() {
    lhnJsSdk.setup = {
      application_id: '2cb55594-3a4a-4b89-bf4b-9f1a6cfdbff3',
      application_secret: '57d499aba56340a4a6916b981ad582f9b98f322ee04641a0bc',
    }
    lhnJsSdk.controls = [
      {
        type: 'hoc',
        id: 'b8ae23fb-4cc3-4b0f-862e-303fb1b57a99',
      },
    ]
    lhnJsSdk.dictionary = {
      agentConnecting: 'Connecting to agent',
      callbackMenu: 'Callback',
      callbackTitle: 'Request a callback',
      cancel: 'Cancel',
      chatMenu: 'Chat',
      chatTitle: '',
      email: 'Email',
      endChat: 'End Chat',
      endChatConfirm: 'Are you sure you want to end the current chat?',
      inviteCancel: 'Dismiss',
      inviteStart: 'Chat now',
      knowledgeMenu: 'FAQ',
      knowledgeTitle: 'Search Knowledge',
      livechat: 'LIVE CHAT',
      livechat_offline: 'GET HELP',
      newChatTitle: 'New Conversation',
      offlineTitle: 'Leave a message',
      send: 'Send',
      startChat: 'Start Chat',
      submit: 'Submit',
      surveyTitle: 'Survey',
      ticketMenu: 'Ticket',
      ticketTitle: 'Submit a ticket',
    }
  }
  ;(function(d, s) {
    var newjs,
      lhnjs = d.getElementsByTagName(s)[0]
    newjs = d.createElement(s)
    newjs.src =
      'https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js'
    lhnjs.parentNode.insertBefore(newjs, lhnjs)
  })(document, 'script')
}

// LHN
const openPreChat = ( $item ) => {
  $item.on("click", () => {
    lhnJsSdk.openHOC()
    updateChat()
  })
}

const updateChat = () => {
  if ($("div.lhnPreChatForm").is(":hidden")) {
    $("#lhnHelpOutCenter").addClass("chat-inchat")
    $("#lhnHelpOutCenter").removeClass("chat-prechat")
    console.log("HIDDEN")
  } else {
    $("#lhnHelpOutCenter").removeClass("chat-inchat")
    $("#lhnHelpOutCenter").addClass("chat-prechat")
    console.log("VISIBLE")
  }
}

export default() => {
  

  // initialize livehelpnow
  initiateLiveHelpNow()  

  $(document).ready(() => { 
    // define const
    const $chatbuttons = $("[data-chat]")
    const $chatWindow = $("#lhnHelpOutCenter")

    // hide on load
    $chatbuttons.hide()

    setInterval(() => {
      const onlineState = !!window.lhsJsSdk || window.lhnJsSdk.isOnline

      if (typeof onlineState === 'undefined') {
        // script is not loaded
        $chatWindow.addClass("chat-undefined")
        return
      }
      if (onlineState) {
        // script is loaded, online
        $chatbuttons.show()
        openPreChat($chatbuttons)
      } else {
        // script is loaded, and we are offline
        $chatbuttons.show()
        $chatbuttons.html("Email Us")
        $chatbuttons.removeAttr("href")
        openPreChat($chatbuttons)
      }
    }, 2000)


  })
}