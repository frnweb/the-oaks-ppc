import $ from 'jquery'
import debounce from 'lodash.debounce'


const addAnimation = ($el, animationClass) => {
	const windowHeight = $(window).height()
	const elementY = $el.offset().top

	if ((windowHeight + window.scrollY) >= elementY) {
		$el.addClass("animated " + animationClass)
	}
}///addAnimation

const elementIterator = (index, item) => {
	const $item = $(item)
	const animationClass = $item.data("in-view")
	addAnimation($item, animationClass)
}///elementIterator

const inView = (selector) => {
	$(document).ready(()=>{
		const $el = $(selector)				
		$el.each(elementIterator)

		$(window).on("scroll", debounce(e =>{
			$el.each(elementIterator)
		}, 5))
	})	
}///inView

	
export default inView