import $ from 'jquery'


const textToggle = () => {
	const $texttoggle = $('[data-toggle="responsive-menu"] .hamburger-label');
	if($texttoggle.text() == 'Menu') {
		$texttoggle.text("Close");
	} else if ($texttoggle.text() == 'Close') {
		$texttoggle.text("Menu");
	}
}///textToggle


const hamburgerToggle = (selector) => {
	const $hamburger = $(selector);
	$hamburger.toggleClass("is-active");
	textToggle()
}///hamburgerToggle


const Hamburger = (selector) => {
	$(document).ready(()=>{
		const $foundationbreakpoint = Foundation.MediaQuery.current
		if($foundationbreakpoint === "small" || $foundationbreakpoint === "medium") {
			$('.dropdown a.button--nav').on('click', function(e) {
				$("#responsive-menu").css("display", "none");
				hamburgerToggle(selector)
			});
		}
		$('[data-toggle="responsive-menu"]').on('click', function(e) {
		  hamburgerToggle(selector)
		});
	})	
}///Hamburger
	
export default Hamburger