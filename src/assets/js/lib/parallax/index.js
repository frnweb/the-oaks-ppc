import $ from 'jquery'
import Rellax from 'rellax'

const Parallax = () => {
	$(document).ready(()=>{
		var rellax = new Rellax('.rellax', {
	    	center: true
	  	});
	})	
}///Parallax
	
export default Parallax