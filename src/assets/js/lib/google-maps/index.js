import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 400
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = +$el.data('map-lat')
  const lng = +$el.data('map-lng')
  const zoom = +$el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = { ...readOptionsFromData($el), ...{ styles } }

      $el.each((index, el) => {
        const $el = $(el)

        loader.then(google => {
          const map = new google.Map(el, options)

          const markerOptions = {
            position: options.center,
            map,
            icon: $el.data('map-marker-url') || null,
            animation: google.Animation.DROP,
          }

          const marker = new google.Marker({ position: options.center, map, icon: "./assets/img/marker_sm.png" })

          const infoWindow = new google.InfoWindow({
            content: `
<div class="map--info">
  <h3>
    The Oaks at La Paloma
  </h3>
  <p>
    2009 Lamar Ave</br>Memphis, TN 38114 
  </p>
  <a href="https://www.google.com/maps/dir/''/the+oaks+at+la+paloma/data=!4m5!4m4!1m0!1m2!1m1!1s0x887f875973a89fab:0x63f3a599c3149029?sa=X&ved=0ahUKEwjj0_e-qLDYAhWiTN8KHa1ZBWkQ9RcImgEwCw" target="_blank" class="button button--directions primary hollow">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
        })
      })
    })
  })
}

export default googleMaps
