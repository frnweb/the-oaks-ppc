import $ from 'jquery'
import Foundation from 'foundation-sites'

export default selector => {
  $(document).ready(() => {
    $(selector)
      .find('a.button--nav')
      .each((index, item) => {
        const $el = $(item)
        const shouldSmoothScroll = /^#.+?$/.test($el.attr('href'))

        if (shouldSmoothScroll) {
          new Foundation.SmoothScroll($el, {
            offset: 50,
            animationEasing: 'swing',
            animationDuration: 700,
            threshold: 100,
          })
        }
      })
  })
}


