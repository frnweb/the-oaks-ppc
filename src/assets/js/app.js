import './lib/dialog-tech'
import $ from 'jquery';
import whatInput from 'what-input'
import googleMaps from './lib/google-maps'
import darkStyle from './lib/google-maps-styles/dark'
import greyStyle from './lib/google-maps-styles/grey'
import Hamburger from './lib/hamburger'
import stickyNav from './lib/sticky-nav'
import smoothScroll from './lib/smooth-scroll'
import Parallax from './lib/parallax'
import inView from './lib/in-view'
import newVisitor from './lib/new-visitor'
import googleAnalytics from './lib/google-analytics'
import liveHelpNow from './lib/live-help-now'
import './lib/hot-jar'


window.$ = $
import Foundation from 'foundation-sites'

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

// ZURB Foundation
$(document).foundation()

// Live Help Now
liveHelpNow()

// Google Analytics
googleAnalytics()

// Google Maps
const stylePicker = style => (style === 'grey' ? greyStyle : null)
googleMaps(stylePicker)


// Hamburger
Hamburger("button.hamburger")

// Parallax
Parallax()

// New Visitor
newVisitor()

// inView
inView("[data-in-view]")

// Sticky Nav
stickyNav("#nav")

// Smooth Scroll
smoothScroll("#nav")



